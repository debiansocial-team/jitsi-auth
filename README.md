# Jitsi.Debian.Social Auth

This is a simple Flask webserver that generates JWT tokens for
https://jitsi.debian.social/.

Users are logged in via Salsa.

## Installation

Create a `config.cfg` file containing OAuth credentials:

```
SITE_NAME = "Debian.social Jitsi Auth"
JITSI_HOSTNAME = "jitsi.debian.social"

SALSA_CLIENT_ID = "..."
SALSA_CLIENT_SECRET = "..."

JITSI_APP_ID = "..."
JITSI_APP_SECRET = "..."
EXPIRY_TIME = 2 * 60 * 60

SECRET_KEY = "..."
```

* `JITSI_APP_ID` is just an identifier string (matching `app_id` in prosody config)
* `JITSI_APP_SECRET` is a high-entropy string to sign JWTs (matching `app_secret` in prosody config)
* `SECRET_KEY` is a high-entropy string to protect session data in cookies.
