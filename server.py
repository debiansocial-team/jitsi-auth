from logging.config import dictConfig
from urllib.parse import urlencode
import time

from flask import Flask, redirect, render_template, session, url_for

from authlib.integrations.flask_client import OAuth
from jwcrypto import jwk, jwt
from werkzeug.middleware.proxy_fix import ProxyFix


dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)

app = Flask(__name__)
app.config.from_pyfile("config.cfg")
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)
oauth = OAuth(app)
oauth.register(
    "salsa",
    server_metadata_url="https://salsa.debian.org/.well-known/openid-configuration",
    client_kwargs={"scope": "openid profile"},
)
key = jwk.JWK.from_password(app.config["JITSI_APP_SECRET"])


@app.context_processor
def inject_config():
    return {
        "JITSI_HOSTNAME": app.config["JITSI_HOSTNAME"],
        "SITE_NAME": app.config["SITE_NAME"],
    }


@app.route("/")
def hello_world():
    return render_template("index.html")


@app.route("/login")
def login():
    redirect_uri = url_for("authorize", _external=True)
    return oauth.salsa.authorize_redirect(redirect_uri)


@app.route("/room_auth/<room_name>")
def room_auth(room_name):
    """Entry point for jitsi-meet's authentication UI"""
    assert not room_name.startswith(".")
    assert "/" not in room_name
    session["room_auth"] = {"room_name": room_name}

    if "salsa_token" in session and "salsa_userinfo" in session:
        return redirect_to_jitsi()

    redirect_uri = url_for("authorize", _external=True)
    return oauth.salsa.authorize_redirect(redirect_uri)


@app.route("/authorize")
def authorize():
    token = oauth.salsa.authorize_access_token()
    session["salsa_token"] = token
    session["salsa_userinfo"] = oauth.salsa.userinfo(token=token)

    if "room_auth" in session:
        return redirect_to_jitsi()
    return redirect("/")


def redirect_to_jitsi():
    room_auth_state = session.pop("room_auth")
    room_name = room_auth_state["room_name"]

    jitsi_token = build_jitsi_jwt(session["salsa_userinfo"], room_name)
    url = (
        "https://"
        + app.config["JITSI_HOSTNAME"]
        + "/"
        + room_name
        + "?"
        + urlencode({"jwt": jitsi_token})
    )
    username = session["salsa_userinfo"]["nickname"]
    user_id = session["salsa_userinfo"]["sub"]
    app.logger.info("Authenticating %s (%s) into room %s", username, user_id, room_name)
    return redirect(url)


def build_jitsi_jwt(userinfo, room_name):
    token = jwt.JWT(
        header={"alg": "HS256", "typ": "JWT"},
        claims={
            "aud": "jitsi",
            "iss": app.config["JITSI_APP_ID"],
            "room": room_name,
            "nbf": int(time.time()),
            "exp": int(time.time()) + app.config["EXPIRY_TIME"],
            "sub": app.config["JITSI_HOSTNAME"],
            "context": {
                "user": {
                    "id": userinfo["nickname"],
                    "name": userinfo["name"],
                    "email": userinfo.get("email"),
                    "avatar": userinfo["picture"],
                },
            },
        },
    )
    token.make_signed_token(key)
    return token.serialize()


@app.route("/logout")
def logout():
    session.clear()
    return redirect("/")
